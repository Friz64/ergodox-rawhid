const St = imports.gi.St;
const Gio = imports.gi.Gio;
const Clutter = imports.gi.Clutter;
const GLib = imports.gi.GLib;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;

class Extension {
    constructor() {
        this._indicator = null;
        this._label = null;
    }

    enable() {
        log(`enabling ${Me.metadata.name}`);

        let indicatorName = `${Me.metadata.name} Indicator`;
        this._indicator = new PanelMenu.Button(0.0, indicatorName, false);
        this._label = new St.Label({
            text: "WPM: ?",
            y_align: Clutter.ActorAlign.CENTER,
            style_class: 'wpm-display'
        });

        this._indicator.add_child(this._label);

        // adding a delay places the indicator on the left
        GLib.timeout_add(GLib.PRIORITY_LOW, 1000, function () {
            Main.panel.addToStatusArea(this._indicator.name, this._indicator, 0);
            return false;
        }.bind(this))
    }

    disable() {
        log(`disabling ${Me.metadata.name}`);

        this._label.destroy();
        this._indicator.destroy();
        this._indicator = null;
    }
}

let serviceObject = null;
let serviceIface = null;
let extensionObject = null;

const ifaceXml = `
<node>
  <interface name="de.friz64.WPMDisplay">
    <method name="UpdateWPM">
      <arg type="y" direction="in" name="wpm"/>
    </method>
  </interface>
</node>`;

class Service {
    UpdateWPM(wpm) {
        extensionObject._label.text = `WPM: ${wpm}`;
    }
}

function init() {
    log(`initializing ${Me.metadata.name}`);

    extensionObject = new Extension();
    serviceObject = new Service();
    serviceIface = Gio.DBusExportedObject.wrapJSObject(ifaceXml, serviceObject);
    serviceIface.export(Gio.DBus.session, '/de/friz64/WPMDisplay');

    return extensionObject;
}
