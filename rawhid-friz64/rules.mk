# https://docs.qmk.fm/#/config_options?id=the-rulesmk-file

LTO_ENABLE = yes
CONSOLE_ENABLE = no
COMMAND_ENABLE = no
WEBUSB_ENABLE = yes
ORYX_ENABLE = yes
RAW_ENABLE = yes
NKRO_ENABLE = yes
MOUSE_SHARED_EP = yes
WPM_ENABLE = yes
