// https://docs.qmk.fm/#/config_options?id=the-configh-file

#define ORYX_CONFIGURATOR
#define USB_SUSPEND_WAKEUP_DELAY 0
#define FIRMWARE_VERSION u8"VYdw7/yNrdx"
#undef RGBLIGHT_ANIMATIONS
#define ERGODOX_LED_30
