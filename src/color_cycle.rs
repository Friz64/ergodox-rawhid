use crate::ergodox::ErgoDox;
use color_eyre::eyre::Result;
use std::num::Wrapping;

const MOVE_AMT: u8 = 2;
const LED_COUNT: usize = 30;

pub struct ColorCycle {
    brightness_buf: [u8; LED_COUNT],
    hue_offset: Wrapping<u8>,
}

impl ColorCycle {
    pub fn new() -> ColorCycle {
        ColorCycle {
            brightness_buf: [0; LED_COUNT],
            hue_offset: Wrapping(0),
        }
    }

    pub fn run_once(&mut self, ergodox: &mut ErgoDox, brightness: f32) -> Result<()> {
        self.hue_offset += Wrapping(MOVE_AMT);

        let brightness = (brightness * u8::MAX as f32).trunc() as u8;
        for val in self.brightness_buf.iter_mut() {
            *val = brightness;
        }

        ergodox.set_hue_cycle(self.hue_offset.0, self.brightness_buf)
    }
}
