mod analyzer;
mod color_cycle;
mod ergodox;

use analyzer::Analyzer;
use color_cycle::ColorCycle;
use color_eyre::eyre::{Result, WrapErr};
use dbus::blocking::{Connection, Proxy};
use ergodox::{Eeprom, ErgoDox, Mode};
use ksni::{
    menu::{RadioGroup, RadioItem, StandardItem},
    Handle, MenuItem, Tray, TrayService,
};
use parking_lot::RwLock;
use std::{fmt, sync::Arc, thread, time::Duration};

impl Mode {
    fn radio_item(self) -> RadioItem {
        RadioItem {
            label: self.to_string(),
            ..Default::default()
        }
    }

    #[allow(clippy::wildcard_in_or_patterns)]
    fn from_eeprom(val: u8) -> Mode {
        match val {
            0 => Mode::White,
            1 => Mode::Analyzer,
            2 => Mode::ColorCycle,
            3 | _ => Mode::Off,
        }
    }
}

impl fmt::Display for Mode {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str(match self {
            Mode::White => "Constant white",
            Mode::Analyzer => "Spectrum analyzer",
            Mode::ColorCycle => "Color cycle",
            Mode::Off => "Off",
        })
    }
}

#[derive(Debug)]
struct Menu {
    eeprom: Eeprom,
    shutdown: bool,
    store_eeprom: bool,
    fetch_eeprom: bool,
}

struct MenuHandle(Arc<RwLock<Menu>>);

impl Tray for MenuHandle {
    fn id(&self) -> String {
        env!("CARGO_CRATE_NAME").into()
    }

    fn icon_name(&self) -> String {
        "input-keyboard-symbolic".into()
    }

    fn scroll(&mut self, delta: i32, _dir: &str) {
        let mut menu = self.0.write();
        let new_brightness = (menu.eeprom.brightness as i32 + delta.signum() * -5) as u8;
        menu.eeprom.brightness = new_brightness.clamp(10, 100);
    }

    fn menu(&self) -> Vec<MenuItem<Self>> {
        let menu = self.0.write();
        vec![
            MenuItem::Standard(StandardItem {
                label: format!("Brightness (scroll): {:3}%", menu.eeprom.brightness),
                enabled: false,
                ..Default::default()
            }),
            MenuItem::Separator,
            MenuItem::RadioGroup(RadioGroup {
                selected: menu.eeprom.mode as usize,
                select: Box::new(|menu: &mut Self, idx| {
                    let mut menu = menu.0.write();
                    menu.eeprom.mode = Mode::from_eeprom(idx as u8);
                }),
                options: vec![
                    Mode::White.radio_item(),
                    Mode::Analyzer.radio_item(),
                    Mode::ColorCycle.radio_item(),
                    Mode::Off.radio_item(),
                ],
            }),
            MenuItem::Separator,
            MenuItem::Standard(StandardItem {
                label: "Store in EEPROM".into(),
                activate: Box::new(|menu| {
                    let mut menu = menu.0.write();
                    menu.store_eeprom = true;
                }),
                ..Default::default()
            }),
            MenuItem::Standard(StandardItem {
                label: "Fetch from EEPROM".into(),
                activate: Box::new(|menu| {
                    let mut menu = menu.0.write();
                    menu.fetch_eeprom = true;
                }),
                ..Default::default()
            }),
            MenuItem::Standard(StandardItem {
                label: "Quit".into(),
                activate: Box::new(|menu| {
                    let mut menu = menu.0.write();
                    menu.shutdown = true;
                }),
                ..Default::default()
            }),
        ]
    }
}

fn run(tray_handle: &mut Handle<MenuHandle>, menu: Arc<RwLock<Menu>>) -> Result<()> {
    let mut ergodox = ErgoDox::new()?;

    menu.write().eeprom = ergodox.fetch_eeprom()?;
    tray_handle.update(|_| ());

    let dbus_conn = Connection::new_session().wrap_err("Failed to instantiate DBus connection")?;
    let dbus_proxy = Proxy::new(
        "org.gnome.Shell",
        "/de/friz64/WPMDisplay",
        Duration::from_millis(5000),
        &dbus_conn,
    );

    let mut analyzer = None;
    let mut color_cycle = None;
    let mut prev_eeprom: Option<Eeprom> = None;
    loop {
        {
            let mut menu = menu.write();
            if menu.shutdown {
                break;
            }

            if menu.store_eeprom {
                menu.store_eeprom = false;
                ergodox.store_eeprom(menu.eeprom)?;
            } else if menu.fetch_eeprom {
                menu.fetch_eeprom = false;
                menu.eeprom = ergodox.fetch_eeprom()?;
                tray_handle.update(|_| ());
            }
        }

        let wpm = ergodox.fetch_wpm()?;
        dbus_proxy.method_call("de.friz64.WPMDisplay", "UpdateWPM", (wpm,))?;

        let eeprom = menu.read().eeprom;
        let mut mode_changed = true;
        let mut brightness_changed = true;
        if let Some(prev_eeprom) = prev_eeprom {
            mode_changed = eeprom.mode != prev_eeprom.mode;
            brightness_changed = eeprom.brightness != prev_eeprom.brightness;
        }

        if mode_changed {
            analyzer = None;
            color_cycle = None;
        }

        let brightness_float = eeprom.brightness as f32 / 100.0;
        match (eeprom.mode, mode_changed || brightness_changed) {
            (Mode::White, true) => {
                let value = (u8::MAX as f32 * brightness_float).trunc() as u8;
                ergodox.set_hsv(0, 0, value)?;
            }
            (Mode::Analyzer, _) => {
                let analyzer = match &mut analyzer {
                    Some(analyzer) => analyzer,
                    None => {
                        analyzer = Some(Analyzer::new()?);
                        analyzer.as_mut().unwrap()
                    }
                };

                analyzer.run_once(&mut ergodox, brightness_float)?;
            }
            (Mode::ColorCycle, _) => {
                let color_cycle = match &mut color_cycle {
                    Some(color_cycle) => color_cycle,
                    None => {
                        color_cycle = Some(ColorCycle::new());
                        color_cycle.as_mut().unwrap()
                    }
                };

                color_cycle.run_once(&mut ergodox, brightness_float)?;
            }
            (Mode::Off, true) => {
                ergodox.set_hsv(0, 0, 0)?;
            }
            _ => (),
        }

        if eeprom.mode != Mode::Analyzer {
            thread::sleep(Duration::from_secs_f32(1.0 / 60.0));
        }

        prev_eeprom = Some(eeprom);
    }

    Ok(())
}

fn main() {
    let menu = Arc::new(RwLock::new(Menu {
        eeprom: Eeprom {
            mode: Mode::Off,
            brightness: 100,
        },
        shutdown: false,
        store_eeprom: false,
        fetch_eeprom: false,
    }));

    let tray_service = TrayService::new(MenuHandle(menu.clone()));

    let mut tray_handle = tray_service.handle();
    tray_service.spawn();

    loop {
        match run(&mut tray_handle, menu.clone()) {
            Ok(()) => break,
            Err(err) => {
                println!("Restarting due to error!\n{:?}\n", err);
                thread::sleep(Duration::from_secs(1));
            }
        }
    }

    tray_handle.shutdown();
}
