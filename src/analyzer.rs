use crate::ergodox::ErgoDox;
use color_eyre::eyre::{Result, WrapErr};
use psimple::Simple;
use pulse::{
    def::BufferAttr,
    sample::{Format, Spec},
    stream::Direction,
};
use spectrum_analyzer::{windows, FrequencyLimit};
use splines::interpolate::Interpolate;
use std::mem;

// Check with: pacmd list-sinks
const PULSE_SINK_NAME: &str = "alsa_output.usb-GeneralPlus_USB_Audio_Device-00.analog-stereo";
const MIN_FREQ: f32 = 50.0;
const MAX_FREQ: f32 = 750.0;
const FRAME_SIZE: usize = 8192;
const LED_COUNT: usize = 30;
const SAMPLE_RATE: usize = 44100;
const UPDATE_RATE: usize = 60;
const CYCLE_SPEED: f32 = 10.0;
const MEDIAN_SMOOTH: f32 = 0.1;
const MIN_SCALE_DIFF: f32 = 3.0;
const OCCURENCE_UPGRADE: f32 = 1.3;
const STEP: usize = SAMPLE_RATE / UPDATE_RATE;
const BYTES_PER_SAMPLE: usize = mem::size_of::<f32>();
const RAW_INPUT_LEN: usize = STEP * BYTES_PER_SAMPLE;
const AUDIO_SPEC: Spec = Spec {
    format: Format::FLOAT32NE,
    channels: 1,
    rate: SAMPLE_RATE as u32,
};

pub struct Analyzer {
    pulse: Simple,
    raw_input_buf: [u8; RAW_INPUT_LEN],
    brightness_buf: [u8; LED_COUNT],
    samples_buf: Vec<f32>,
    hue_offset: f32,
}

impl Analyzer {
    pub fn new() -> Result<Analyzer> {
        assert!(AUDIO_SPEC.is_valid());
        let pulse = Simple::new(
            None,
            env!("CARGO_PKG_NAME"),
            Direction::Record,
            Some(&(PULSE_SINK_NAME.to_string() + ".monitor")),
            "Arbitrary Audio Data",
            &AUDIO_SPEC,
            None,
            Some(&BufferAttr {
                maxlength: RAW_INPUT_LEN as u32,
                tlength: u32::MAX,
                prebuf: u32::MAX,
                minreq: u32::MAX,
                fragsize: RAW_INPUT_LEN as u32,
            }),
        )
        .wrap_err("Failed to connect to pulse")?;

        Ok(Analyzer {
            pulse,
            raw_input_buf: [0; RAW_INPUT_LEN],
            brightness_buf: [0; LED_COUNT],
            samples_buf: vec![0.0; FRAME_SIZE],
            hue_offset: 0.0,
        })
    }

    pub fn run_once(&mut self, ergodox: &mut ErgoDox, brightness: f32) -> Result<()> {
        self.pulse
            .read(&mut self.raw_input_buf)
            .wrap_err("Failed to read data from PA")?;

        self.samples_buf.drain(..STEP);
        self.samples_buf
            .extend_from_slice(bytemuck::cast_slice(&self.raw_input_buf));

        let hann_window = windows::hann_window(&self.samples_buf);
        let spectrum = spectrum_analyzer::samples_fft_to_spectrum(
            &hann_window,
            SAMPLE_RATE as u32,
            FrequencyLimit::Range(MIN_FREQ, MAX_FREQ),
            Some(&move |x, stats| {
                (Interpolate::lerp(MEDIAN_SMOOTH, x, stats.median) - stats.min)
                    / stats.max.max(MIN_SCALE_DIFF)
            }),
        )
        .expect("Spectrum analysis failed");

        let min_freq = spectrum.min_fr().val();
        let max_freq = spectrum.max_fr().val();
        let freq_diff = max_freq - min_freq;
        let freq_step = freq_diff / LED_COUNT as f32;

        let mut bin_idx = 0;
        let mut freq_vals = vec![];
        for (freq, freq_val) in spectrum.data().iter() {
            let freq = freq.val();
            let mut freq_val = freq_val.val();

            if bin_idx >= LED_COUNT / 2 {
                freq_val = (freq_val * OCCURENCE_UPGRADE).min(1.0);
            }

            let freq_bin_max = min_freq + freq_step * bin_idx as f32;
            if freq > freq_bin_max {
                freq_vals.push(freq_val);
                let led_idx = LED_COUNT - 1 - bin_idx;
                let average_brightness = freq_vals.iter().sum::<f32>() / freq_vals.len() as f32;
                self.brightness_buf[led_idx] =
                    (average_brightness * u8::MAX as f32 * brightness) as u8;

                freq_vals.clear();
                bin_idx += 1;
            } else {
                freq_vals.push(freq_val);
            }
        }

        self.hue_offset = (self.hue_offset + spectrum.median().val() * CYCLE_SPEED) % 256.0;
        ergodox.set_hue_cycle(self.hue_offset.trunc() as u8, self.brightness_buf)
    }
}
