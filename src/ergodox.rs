use color_eyre::eyre::{Result, WrapErr};
use hidapi::{HidApi, HidDevice};

const MAX_DATA_LEN: usize = 32;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Mode {
    White,
    Analyzer,
    ColorCycle,
    Off,
}

#[derive(Debug, Clone, Copy)]
pub struct Eeprom {
    pub mode: Mode,
    pub brightness: u8,
}

pub struct ErgoDox {
    _hidapi: HidApi,
    dev: HidDevice,
    buf: [u8; MAX_DATA_LEN + 1],
}

impl ErgoDox {
    pub fn new() -> Result<ErgoDox> {
        let hidapi = HidApi::new().wrap_err("Failed to init hidapi")?;
        let dev = hidapi
            .device_list()
            .find(|info| {
                info.vendor_id() == 0x3297
                    && info.product_id() == 0x4975
                    && info.interface_number() == 1
            })
            .expect("Failed to find matching ErgoDox")
            .open_device(&hidapi)
            .wrap_err("Failed to open ErgoDox")?;
        dev.set_blocking_mode(true)
            .wrap_err("Failed to set blocking mode")?;

        Ok(ErgoDox {
            _hidapi: hidapi,
            dev,
            buf: [0; MAX_DATA_LEN + 1],
        })
    }

    fn send_request(&mut self, request_id: u8, bytes: &[u8]) -> Result<()> {
        assert!(bytes.len() < MAX_DATA_LEN);
        self.buf[0] = 0;
        self.buf[1] = request_id;
        self.buf[2..bytes.len() + 2].copy_from_slice(bytes);
        self.dev.write(&self.buf).wrap_err("Write data failed")?;
        Ok(())
    }

    fn recv_response(&mut self) -> Result<&[u8]> {
        let read_len = self
            .dev
            .read(&mut self.buf[1..])
            .wrap_err("Read data failed")?;

        assert_eq!(read_len, MAX_DATA_LEN);
        Ok(&self.buf[1..])
    }

    pub fn set_hsv(&mut self, hue: u8, saturation: u8, value: u8) -> Result<()> {
        self.send_request(1, &[hue, saturation, value])
    }

    pub fn set_hue_cycle(&mut self, hue_offset: u8, brightness_values: [u8; 30]) -> Result<()> {
        let mut bytes = [0; 31];
        bytes[0] = hue_offset;
        bytes[1..].copy_from_slice(&brightness_values);

        self.send_request(2, &bytes)
    }

    pub fn fetch_wpm(&mut self) -> Result<u8> {
        self.send_request(3, &[])?;
        Ok(self.recv_response()?[1])
    }

    pub fn fetch_eeprom(&mut self) -> Result<Eeprom> {
        self.send_request(4, &[])?;
        let resp = self.recv_response()?;

        Ok(Eeprom {
            mode: Mode::from_eeprom(resp[1]),
            brightness: resp[2].clamp(10, 100),
        })
    }

    pub fn store_eeprom(&mut self, eeprom: Eeprom) -> Result<()> {
        self.send_request(5, &[eeprom.mode as u8, eeprom.brightness])
    }
}
