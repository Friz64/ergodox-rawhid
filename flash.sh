#!/bin/sh
set -e

KEYMAP="$1"

cd $(qmk config user.qmk_home | awk -F '=' '{print $2}')
qmk compile -kb ergodox_ez/shine -km $KEYMAP -e AVR_CFLAGS="-Wno-array-bounds"
wally-cli "ergodox_ez_shine_$KEYMAP.hex"
