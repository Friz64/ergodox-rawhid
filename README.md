# ergodox-rawhid

Personalized `Linux GNOME Desktop` <-> `ErgoDox EZ Shine` interface.

Read the blog post [here](https://blog.friz64.de/ergodox-rawhid)!

## Compilation/Usage

- Computer client

```
cargo run --release
```

- Keyboard firmware

```
./flash.sh rawhid-friz64
```

## My QMK Setup

1. Setup the ZSA QMK firmware.

```
qmk setup zsa/qmk_firmware -b firmware20 --home $HOME/.qmk_firmware_zsa
```

2. Symlink `~/.qmk_firmware_zsa/keyboards/ergodox_ez/keymaps/rawhid-friz64` to `./rawhid-friz64`.
